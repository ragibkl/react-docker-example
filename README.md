# React Docker Example

An example of deploying a React App production build using Docker.

This purpose of this project is to demonstrate how to deploy a React App production build on a remote server using *Docker* and *Docker Compose*.

Let's say, you have been following the various React tutorials and walkthroughs online, and have prototyped a React project for yourself.
You have been running everything on your local development environment all this while.
Now, you are ready to deploy your React App on a remote server for public access. So, how do we do this?

If you find yourself asking this question, then this guide is for you.

If you want to test this method yourself, you can clone this repo and follow along this document.
Once you are comfortable with the concepts, feel free to copy the scripts and files from this repo into your own projects, and adapt them as needed.

## Create React App V2

This project was bootstrapped with version 2 of [Create React App](https://github.com/facebook/create-react-app).
You can refer to the most recent version of CRA documentation guide [here](https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md).

## Getting Started

As with most CRA2 projects, you need to have node and npm installed on your machine.
Then you simply clone the repo, and run the required scripts as follows:
```
git clone [repo link]
cd [folder]

npm install
npm start
```

You should then be able to edit the files in `src/` to develop the app. Please refer to other React tutorials and documentations for more info.
For other React App related stuffs, please refer to the CRA documentation [here](https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md).

## Deployment using Docker and Docker Compose

### Server setup

1. Provision a VPS server with a public IP. My favourite is Digitalocean.
This project uses Ubuntu 16.04 LTS as an example.
Make sure that you can ssh into the server and run commands on it.
2. Point a public domain name to the public IP of the server. If you need a free domain to use, check out cloudns.net
```
my-react-app.example.com -> [server-ip]
```

### Local Setup

1. Install docker on your machine
2. Login to Gitlab Docker registry service
```
docker login registry.gitlab.com
```
3. Create a copy of the file `scripts/.env.sample`. Update the content as needed.
```
cp scripts/.env.sample scripts/.env
```
4. Run the server-install script to setup `docker` and `docker-compose` on the server. This was tested on ubuntu 16.04 lts. If it does not work, you might have to do it manually. Refer to the script for reference.
```
./scripts/server-install.sh
```
You will be asked for a username and password to register to `registry.gitlab.com`. I recommend creating a read-only deploy token for your repo, with `read_registry` permission.

### Build and Deploy

1. Run the build script to build a docker image of your React App. This script also pushes the image to docker registry.
```
./scripts/build.sh
```
2. Run the deploy script to deploy it to production.
```
./scripts/server-deploy.sh
```
