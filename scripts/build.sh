#!/usr/bin/env bash
source scripts/.env
TAG=${1:-latest}

echo $IMAGE_REGISTRY:$TAG

# Build the react app, and copy to docker folder
npm run build
cp -r build/. docker/react-web/build

docker build -t $IMAGE_REGISTRY:$TAG docker/react-web
docker push $IMAGE_REGISTRY:$TAG

# Clean up the build artifacts
rm -rf build
rm -rf docker/react-web/build
