#!/usr/bin/env bash
# deploy latest image to server

source scripts/.env

scp -r docker/deploy/. $SERVER_LOGIN:~/deploy
./scripts/ssh-run.sh $SERVER_LOGIN ./scripts/server-scripts/deploy.sh $DOMAIN $EMAIL $IMAGE_REGISTRY
