#!/usr/bin/env bash
# Installs docker and docker-compose on a remote Ubuntu 18.04 server

source scripts/.env
./scripts/ssh-run.sh $SERVER_LOGIN scripts/server-scripts/install.sh
