#!/usr/bin/env bash

DOMAIN=$1
EMAIL=$2
IMAGE_REGISTRY=$3

cd ~/deploy
DOMAIN=$DOMAIN EMAIL=$EMAIL IMAGE_REGISTRY=$IMAGE_REGISTRY docker-compose pull
DOMAIN=$DOMAIN EMAIL=$EMAIL IMAGE_REGISTRY=$IMAGE_REGISTRY docker-compose up -d

# cleanup
cd
rm -rf deploy
