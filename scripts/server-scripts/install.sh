#!/usr/bin/env bash
# Ubuntu 18.04 docker and docker-compose install script

# install dependencies
apt-get update
apt-get install -y \
  apt-transport-https \
  ca-certificates \
  curl \
  python3-pip \
  software-properties-common

# add docker repository
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

# install docker and docker-compose
apt-get update
apt-get install docker-ce -y
pip3 install --upgrade docker-compose pip

# Login to gitlab docker registry
echo "logging in to GitLab docker registry. [registry.gitlab.com]"
echo "Please use a READ-ONLY server deploy key for this."
docker login registry.gitlab.com

# also performs software upgrade and reboot for good measure
apt-get upgrade -y
reboot
