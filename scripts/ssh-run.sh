#!/usr/bin/env bash
# Runs a local script inside a remote server
# USAGE: ./scripts/ssh-run.sh [SERVER_LOGIN] [path/to/script.sh] [arguments]

SERVER_LOGIN=$1
shift
FILE=$1
shift
ARGS=$*

scp $FILE $SERVER_LOGIN:/tmp/script.sh
ssh -t $SERVER_LOGIN "chmod u+x /tmp/script.sh; /tmp/script.sh $ARGS"
